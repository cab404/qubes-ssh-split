# Qubes SSH split

Makes SSH agent from one of your domains available on the other.

(this repo was pushed using that thing)

# Installation

0. Add some SSH keys on a vault domain. Remove passwords from them.

1. Copy `qubes-ssh-split-install` to your vault domain and invoke it in `/rw/config/rc.local`. 

2. Copy `qubes-ssh-agent` to some other domain.

3. Install `nc` on that domain. 

4. `export $QUBES_SSH_SPLIT=vault_domain`, where `vault_domain` is your vault domain .-. (set to `vault` by default)

5. ``eval `./qubes-ssh-agent` ``

6. Try ssh-ing somethere.


